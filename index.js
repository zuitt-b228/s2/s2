//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],

    // add functionalities
        // keywords "this" refers to the object encapsulating the method where "this" is called
        // encapsulation - organizes related information(properties) and behavior (methods) to belong to a single entity
    /*
        login(){
            console.log(`${this.email} has logged in`)
        }
        logout(){
            console.log(`${this.email} has logged out`)
        }
        login(){
            console.log(`Student one's grade averages are ${this.grades}`)
        }
    */
    /*login: function (add){
        console.log(`${add} has logged in`);
    },

    logout: function (add){
        console.log(`${add} has logged out`);
    },

    listGrades: function(grd){
        grd.forEach(grade => {
            console.log(grade);
        })
    },*/
    login: function (){
        console.log(`${this.name} has logged in`);
    },

    logout: function (){
        console.log(`${this.name} has logged out`);
    },

    listGrades: function(){
        console.log(`Student two's grade averages are ${this.grades}`)
    },
    computeAve: function(){
        grades = this.grades
        let sum = grades.reduce((a, b) => a + b)
        let avg = sum / grades.length
        return avg
    },
    willPass: function(){
        let ave = this.computeAve()
        if(ave >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        let ave = this.computeAve()
        if(ave >= 90){
            return true
        } else if (ave >= 85 && ave <90){
            return false
        } else if(ave <85) {
            return undefined
        }
    }
}

// console.log(`Student one's name is ${studentOne.name}`);
// console.log(`Student one's email is ${studentOne.email}`);
// console.log(`Student one's grade averages are ${studentOne.grades}`);
/* Mini-Activity
    insert the methods login, logout, listgrades inside studentOne and display the appropriate message when they are invoked
 */
// console.log(studentOne.login(studentOne.email));
// console.log(studentOne.logout(studentOne.email));
// console.log(studentOne.listGrades(studentOne.grades));

/*  Mini-Activity
    - encapsulate the remaining students (studentTow, studenThree and studentFour) with login, logout and listGrades methods just like studentOne
*/



let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],

    login: function (){
        console.log(`${this.name} has logged in`);
    },

    logout: function (){
        console.log(`${this.name} has logged out`);
    },

    listGrades: function(){
        console.log(`Student two's grade averages are ${this.grades}`)
    },
    computeAve: function(){
        grades = this.grades
        let sum = grades.reduce((a, b) => a + b)
        let avg = sum / grades.length
        return avg
    },
    willPass: function(){
        let ave = this.computeAve()
        if(ave >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        let ave = this.computeAve()
        if(ave >= 90){
            return true
        } else if (ave >= 85 && ave <90){
            return false
        } else if(ave <85) {
            return undefined
        }
    }
}

let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],

    login: function (){
        console.log(`${this.name} has logged in`);
    },

    logout: function (){
        console.log(`${this.name} has logged out`);
    },

    listGrades: function(){
        console.log(`Student two's grade averages are ${this.grades}`)
    },
    computeAve: function(){
        grades = this.grades
        let sum = grades.reduce((a, b) => a + b)
        let avg = sum / grades.length
        return avg
    },
    willPass: function(){
        let ave = this.computeAve()
        if(ave >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        let ave = this.computeAve()
        if(ave >= 90){
            return true
        } else if (ave >= 85 && ave <90){
            return false
        } else if(ave <85) {
            return undefined
        }
    }
}

let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],

    login: function (){
        console.log(`${this.name} has logged in`);
    },

    logout: function (){
        console.log(`${this.name} has logged out`);
    },

    listGrades: function(){
        console.log(`Student two's grade averages are ${this.grades}`)
    },

    computeAve: function(){
        grades = this.grades
        let sum = grades.reduce((a, b) => a + b)
        let avg = sum / grades.length
        return avg
    },
    willPass: function(){
        let ave = this.computeAve()
        if(ave >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function(){
        let ave = this.computeAve()
        if(ave >= 90){
            return true
        } else if (ave >= 85 && ave <90){
            return false
        } else if(ave <85) {
            return undefined
        }
    }
}

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents: function(){
        let arr = this.students
        let counter = 0;
        arr.forEach(element => {
            if(element.willPassWithHonors() === true){
                counter++;
            }
        })
        return counter;
    },
    honorsPercentage: function(){
        let countStudents = this.countHonorStudents()
        let percentage = (countStudents/this.students.length)*100
        return `${percentage}%`;
    },
    retrieveHonorStudenInfo: function(){
        let arr = this.students
        let studentInfo = []
        arr.forEach(element => {
            if(element.willPassWithHonors() === true){
                studentInfo.push([element.computeAve(), element.email])
            }
        })
        return studentInfo;
    },
    sortHonorStudentsByGradeDesc: function(){
        let retrieved = this.retrieveHonorStudenInfo()
        retrieved.sort()
        return retrieved.reverse()
    }
}


// ============================================================================================== //

// *** QUIZ *** //

// What is the term given to unorganized code that's very hard to work with?
    // spaghetti code

// How are object literals written in JS?
    // {}

// What do you call the concept of organizing information and functionality to belong to an object?
    //

// If the studentOne object has a method named enroll(), how would you invoke it?
    // studentOne.enroll()

// True or False: Objects can have objects as properties.
    // TRUE

// What is the syntax in creating key-value pairs?
    // key: value

// True or False: A method can have no parameters and still work.
    // TRUE


// True or False: Arrays can have objects as elements.
    // FALSE

// True or False: Arrays are objects.
    // FALSE

// True or False: Objects can have arrays as properties.
    // TRUE


// *** FUNCTION CODING *** // 

/*
    Activity:

    1. Translate the other students from our boilerplate code into their own respective objects.

    2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

    3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

    4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

    5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

    6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

    7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

    8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

    9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
*/


























